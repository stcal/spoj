#include <iostream>
#include <vector> 
 
using namespace std;
 
struct point
{
    int m_iX;
    int m_iY;
};
 
uint64_t fieldx2(vector<point> a_vPoints)
{
    uint64_t iField = 0ull;
 
    for(int i=0; i+1 < a_vPoints.size(); i++)
    {
        iField = iField + a_vPoints[i].m_iX*a_vPoints[i+1].m_iY - a_vPoints[i+1].m_iX*a_vPoints[i].m_iY;
    }
    if(0>iField)
    {
        iField = 0;
    }
    return iField;
}
 
uint64_t count_paint(vector<point> a_vPointsBlack, vector<point> a_vPointsGrey)
{
    uint64_t iPaint = 0ll;
 
    uint64_t iF2Grey = fieldx2(a_vPointsGrey);
    uint64_t iF2Black = fieldx2(a_vPointsBlack);
 
    iPaint = iF2Grey*3 + iF2Black*2;
 
    return iPaint;
}
 
void fill_points(vector<point> &a_rPoints)
{
    point temp;
    do
    {
        std::cin >> temp.m_iX;
        std::cin >> temp.m_iY;
        a_rPoints.push_back(temp);
    }
    while(!(1<a_rPoints.size() && a_rPoints[0].m_iX==temp.m_iX && a_rPoints[0].m_iY==temp.m_iY));
}
 
int main()
{
    int iDrawingsNumber = 0;
 
    vector<uint64_t> paintNeeded;
 
    vector<point> blackPoints;
    vector<point> greyPoints;
 
    std::cin >> iDrawingsNumber;
 
    for(int i = 0; i < iDrawingsNumber; i++)
    {
        fill_points(blackPoints);
        fill_points(greyPoints);
 
        paintNeeded.push_back(count_paint(blackPoints,greyPoints));
 
        blackPoints.clear();
        greyPoints.clear();
    }
 
    for(int i = 0; i < iDrawingsNumber; i++)
    {
        std::cout << paintNeeded[i] << std::endl;
    }
 
    return 0;
}
 