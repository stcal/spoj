#include <iostream>
#include <string>
#include <vector>

using namespace std;

void wiggle(vector<vector<uint> > & vOutput, uint uDestLen, uint uStringLength)
{
    bool blocked=true;
    static uint arr[1000];
    vector<uint> vWorkingVector;
    uint uCurrentIndex=0u;
    uint uRemovedLetters = uStringLength - uDestLen;
    
    //for first try
    for(uint i=0u; i<uRemovedLetters; i++)
    {
        arr[i]=i;
        vWorkingVector.push_back(i);
    }
    vOutput.push_back(vWorkingVector);
    vWorkingVector.clear();
    
    do
    {
        //is blocked
        blocked=true;
        if(0u>=uRemovedLetters)
        {
            break;
        }
        //start from the last field
        uCurrentIndex=(uRemovedLetters-1u);
        
        while(
            //not the first field
            0u!=uCurrentIndex &&
            //field's value reached max 
            (((uStringLength-1u)==arr[uCurrentIndex]) || 
            //field is not last, but cannot be increased
            ((uRemovedLetters-1u)!=uCurrentIndex && (arr[uCurrentIndex]+1u)==(arr[uCurrentIndex+1u]))))
        {
            //move left
            uCurrentIndex--;
        }
        //check if value can be increased
        if((arr[uCurrentIndex]+1u)!=(arr[uCurrentIndex+1u])&&(uStringLength-1u)!=arr[uCurrentIndex])
        {
            //not blocked
            blocked=false;
        }
        
        //if moves possible
        if(false==blocked)
        {
            //rise value 
            arr[uCurrentIndex]++;
            //adjust value of following fields
            for(int i=uCurrentIndex+1u; i<uRemovedLetters; i++)
            {
                arr[i]=arr[i-1u]+1u;
            }
            
            //print content
            for(uint i=0u; i<uRemovedLetters; i++)
            {
                vWorkingVector.push_back(arr[i]);
            }
            vOutput.push_back(vWorkingVector);
            vWorkingVector.clear();
        }
    }
    while(false==blocked);
}

bool comm(string & A, int iLenA, string & B, uint iLenB, uint len)
{    
    bool found = false;
    int uu=0;
    int oo=0;
    
    string sCopyA;
    string sCopyB;
    
    vector<vector<uint> > resultsA;
    vector<vector<uint> > resultsB;
    
    wiggle(resultsA,len,iLenA);
    wiggle(resultsB,len,iLenB);
    
    for(vector<uint> a : resultsA)
    {
        sCopyA=A;
        uu=0;
        for(uint y : a)
        {
            sCopyA.erase(sCopyA.begin()+y-uu);
            uu++;
        }
        for(vector<uint> b : resultsB)
        {
            sCopyB=B;
            oo=0;
            for(uint x : b)
            {
                sCopyB.erase(sCopyB.begin()+x-oo);
                oo++;
            }
            if(sCopyA==sCopyB)
            {
                found = true;
                break;
            }
        }
        if(true==found)
        {
            break;
        }
    }
    
    resultsA.clear();
    resultsB.clear();
    
    return found;
}

int main()
{
    uint max=0;

    string sFirst;
    string sSecond;
    uint iFirstLen=0u;
    uint iSecondLen=0u;

    uint iNumberOfTests=0;
    cin >> iNumberOfTests;

    for(uint i=0u; i<iNumberOfTests; i++)
    {
        cin >> iFirstLen;
        cin >> sFirst;
        cin >> iSecondLen;
        cin >> sSecond;
    
        max = (iFirstLen>=iSecondLen) ? iSecondLen : iFirstLen;
    
        while(max>0)
        {
            if(sFirst==sSecond)
            {
                break;    
            }
            
            if(false==comm(sFirst, iFirstLen, sSecond, iSecondLen, max))
            {
                max--;       
            }
            else
            {
                break;
            }
        }
        cout << max << endl;
    }

    return 0;
}