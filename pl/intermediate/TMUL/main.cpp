#include <iostream>

using namespace std;

int getVal(int* Arr)
{
    int digits=0;
    
    while(Arr[digits] = getchar())
    {
        if(EOF==Arr[digits]||32==Arr[digits]||13==Arr[digits]||10==Arr[digits])
        {
            break;
        }
        Arr[digits] -= 48;
        digits++;
    }
    return digits;
}

void multip(int* sum, int* A, int digitsInA, int* B, int digitsInB)
{
    for(int aSubstratDigit=digitsInA-1; aSubstratDigit>=0; aSubstratDigit--)
    {
    	for(int bSubstratDigit=digitsInB-1; bSubstratDigit>=0; bSubstratDigit--)
    	{
    	    int idx = (bSubstratDigit+aSubstratDigit+1);
    		sum[idx] += A[aSubstratDigit]*B[bSubstratDigit];
    		sum[idx-1] += sum[idx]/10;
    		sum[idx] = sum[idx]%10;
    	}
    }
}

int main()
{
    int valueX = 0;
    int valueY = 0;
    int sizeX = 0;
    int sizeY = 0;
    
    int x[10000]={0};
    int y[10000]={0};
    int result[20000]={0};
    bool zero = true;
        
    int repeats=0;
    
    cin >> repeats;
    
    getchar();
    
    for(int tryNumb=0; tryNumb<repeats; tryNumb++)
    {
        zero = true;
        for(int i=0; i<20000; i++)
        {
            result[i]=0;
        }
        
        sizeX = getVal(x);
        sizeY = getVal(y);
        
        multip(result,x,sizeX,y,sizeY);
        
        for(int i=0; i<(sizeX+sizeY); i++)
        {
            if(true!=zero || 0!=result[i] || (sizeX+sizeY-1)==i)
            {
                zero = false;
                cout << result[i];
            }
        }
        cout << endl;
    }
    
    return 0;
}
