#include <iostream>
#include <sstream> 
#include <string>
#include <iomanip>

using namespace std;

string turnEleven(int arg)
{
    stringstream buffer;
    string out;
    int temp;
    for(int i=0;arg>0;i++)
    {
        temp = arg%11;
        arg=(arg-temp)/11;
        buffer.str(string());
        buffer << hex << uppercase << temp;
        out.insert(0,buffer.str());
    }
    return out;
}

int main()
{
    int iAmount=0;
    int temp;
    cin >> iAmount;
    for(int i=0;i<iAmount;i++)
    {
        cin >> temp;
        cout << hex << uppercase << temp << " " << turnEleven(temp) << endl;
    }

    return 0;
}