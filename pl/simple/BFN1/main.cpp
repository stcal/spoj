#include <iostream>
#include <vector>

using namespace std;

struct aPair
{
    int a;
    int b;
};

int invertNumb(int a_iNumber)
{
    int iDigits=0;
    int iTemp=0;
    int iLimes=1;
    
    int iOutput = 0;
    
    //counting digits in number
    do
    {
        iDigits++;
        iLimes *= 10;
        iTemp = a_iNumber%iLimes;
    }
    while(iTemp != a_iNumber);
    
    //digits inversion
    for(int iPosition=10;iPosition<=iLimes;iPosition*=10)
    {
        iTemp = a_iNumber%iPosition/(iPosition/10);
        a_iNumber=a_iNumber-iTemp;
        iOutput += (iLimes/iPosition)*iTemp;
    }
    
    return iOutput;
}

int pali(int a_iNumber)
{
    if(invertNumb(a_iNumber) != a_iNumber)
    {
        a_iNumber += invertNumb(a_iNumber);
    }
    return a_iNumber;
}

int main()
{
    int iNumber=0;
    int iTests=0;
    int iTemp=0;
    aPair vTemp;
    
    vector <aPair> vOut;
    
    bool bDone = false;
    int repeat = 0;
    
    cin >> iTests;
    
    for(int i=0; i<iTests; i++)
    {
        cin >> iNumber;
        // vTemp.a=iNumber;
        // vTemp.b=repeat-1;
        // vOut.push_back(vTemp);
        do
        {
            iTemp = pali(iNumber);
            repeat++;
            if(iNumber == iTemp)
            {
                bDone = true;
            }
            iNumber = iTemp;
        }
        while(!bDone);
        
        vTemp.a=iNumber;
        vTemp.b=repeat-1;
        vOut.push_back(vTemp);
        
        bDone = false;
        repeat = 0;
    }

    for(aPair A : vOut)
    {
        cout << A.a << " " << A.b << endl;
    }

    return 0;
}