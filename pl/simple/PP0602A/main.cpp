#include <iostream>

using namespace std;

int main()
{
    int iTestsNumber=0;
    int iElements=0;
    int iArray[100];
    int iOdd[50];
    int iEven[50];
    int iEvenCnt=0;
    int iOddCnt=0;
    
    cin >> iTestsNumber;
    
    for(int iTest=0; iTest<iTestsNumber; iTest++)
    {
        cin >> iElements;
        for(int iElement=0;iElement<iElements;iElement++)
        {
            if(1!=iElement%2)
            {
                cin >> iOdd[iOddCnt];
                iOddCnt++;
            }
            else
            {
                cin >> iEven[iEvenCnt];
                iEvenCnt++;
            }
        }

        for(int iElement=0;iElement<iEvenCnt;iElement++)
        {
            cout << iEven[iElement] << " ";
        }
        for(int iElement=0;iElement<iOddCnt;iElement++)
        {
            cout << iOdd[iElement] << " ";
        }
        cout << endl;
        iEvenCnt=0;
        iOddCnt=0;
    }
    
    return 0;
}