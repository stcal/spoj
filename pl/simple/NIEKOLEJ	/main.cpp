#include <iostream>

using namespace std;

void kombo(int o)
{
    if(2>=o)
    {
        cout << "NIE" << endl;
    }
    else
    {
        int h=0;
        h = o/2;
        
        int i=h+1;
        int j=0;
        
        do
        {
            if(j<=h)
            {
                cout << j << " ";
                // break;
                j++;
            }
            if(i<=o)
            {
                cout << i << " ";
                // break;
                i++;
            }
        }while(j<=h || i<=o);
        cout << endl;
    }
}

int main()
{
    int arg=0;
    
    cin >> arg;
    
    kombo(arg);

    return 0;
}