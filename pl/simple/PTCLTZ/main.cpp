#include <iostream>

using namespace std;

int collatz(int s)
{
    int iX=s;
    int n=0;
    while(1!=iX)
    {
        if(0!=iX%2)
        {
            iX=1+3*iX;
        }
        else
        {
            iX=iX/2;
        }
        n++;
    }
    return n;
}

int main()
{
    int iTestsNumber = 0;
    int Temp=0;
    cin >> iTestsNumber;
    
    for(int iTest=0;iTest<iTestsNumber;iTest++)
    {
        cin >> Temp;
        cout << collatz(Temp) << endl;
        
    }
    
    return 0;
}