#include <iostream>
#include <vector>

using namespace std;

struct aPair
{
    uint64_t base;
    uint64_t power;
};

int power(int a_iBase, int a_iPower)
{
    int ret = 0; 
    
    if((1 == a_iPower)|(1 >= a_iBase))
    {
        ret = a_iBase;
    }
    else
    {
        ret = (a_iBase*power((a_iBase), (a_iPower-1))) % 10;
    }
    
    return ret;
}

int main()
{
    int amount;
    aPair temp;
    int iPower = 0;
        
    vector<aPair> numbers;
    cin >> amount;
    
    for(int i = 0; i<amount; i++)
    {
        cin >> temp.base;
        cin >> temp.power;
        numbers.push_back(temp);
    }
    
    for(aPair a : numbers)
    {
        if(a.power )
        a.power = (a.power > 4) ? ((a.power-1)%4ull)+1 : a.power;
        iPower = power(static_cast<int>(a.base%10ull), static_cast<int>(a.power));
        
        cout << iPower << endl;
    }

    return 0;
}