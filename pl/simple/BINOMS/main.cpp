#include <iostream>

using namespace std;

uint64_t power(uint64_t arg, uint64_t lim)
{
    uint64_t out;
    if(lim<arg)
    {
        out = arg * power(arg-1,lim);
    }
    else
    {
        out = 1;
    }
    return out;
}

uint64_t combination(uint64_t aN, uint64_t aK)
{
    uint64_t iOut;
    iOut = ((aK>(aN-aK))? (power(aN,aK)/power(aN-aK,1)) : (power(aN,aN-aK)/power(aK,1)));
    return iOut;
}

int main()
{
    int iTestsNumber=0;
    int64_t iTempN=0;
    int64_t iTempK=0;
    
    cin >> iTestsNumber;
    
    for(int iTest=0;iTest<iTestsNumber;iTest++)
    {
        cin >> iTempN;
        cin >> iTempK;
        if(0>iTempK || 0>iTempN || iTempK>iTempN)
        {
            cout << 0 << endl;
        }
        else
        {
            cout << combination(static_cast<uint64_t>(iTempN),static_cast<uint64_t>(iTempK)) << endl;
        }
    }

    return 0;
}