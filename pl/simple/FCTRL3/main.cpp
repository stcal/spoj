#include <iostream>
#include <vector>

using namespace std;

int power(int arg)
{
    int ret = 0; 
    if(1 >= arg)
    {
        ret = 1;
    }
    else
    {
        ret = (arg*power(arg-1));
    }
    return ret;
}

int main()
{
    int amount;
    int temp;
    vector<int> numbers;
    cin >> amount;
    
    for(int i = 0; i<amount; i++)
    {
        cin >> temp;
        numbers.push_back(temp);
    }
    
    for(int a : numbers)
    {
        int iPower = 0;
        int dec = 0;
        int sing = 0;
        
        if(100 > a)
        {
            iPower = power(a);
            
            sing = iPower%10;
            dec = (iPower%100 - sing) / 10;
        }
        
        cout << dec << " " << sing << endl;
    }

    return 0;
}