#include <iostream>

using namespace std;

int main()
{
    int iTestsAmount=0;
    int iAmount=0;
    int iNumbers[100];
    
    cin >> iTestsAmount;
    
    for(int iTest=0; iTest<iTestsAmount; iTest++)
    {
        cin >> iAmount;
        for(int iElement=0;iElement<iAmount;iElement++)
        {
            cin >> iNumbers[iElement];
        }
        for(int iElement=1;iElement<=iAmount;iElement++)
        {
            if(iAmount==iElement)
            {
                iElement=0;
            }
            cout << iNumbers[iElement] << " ";
            if(0==iElement)
            {
                break;
            }
        }
        cout << endl;
    }
    return 0;
}