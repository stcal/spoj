#include <iostream>

#define M_PI 3.141592654

using namespace std;

double field(double d, double r)
{
    double ret=0;
    if(d<(2*r))
    {
        ret = M_PI*(r*r-(d*d/4));
    }
    return ret;
}

int main()
{
    double iD;
    double iR;
    
    cin >> iR;
    cin >> iD;
    
    printf("%.2f",field(iD,iR));

    return 0;
}