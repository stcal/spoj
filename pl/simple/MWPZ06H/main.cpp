#include <iostream>

using namespace std;

void swap(int* a, int* b)
{
    int temp;
    temp = *a;
    *a = *b;
    *b = temp;
}

int main()
{
    // int arr[] = {1,2,3,4,5,6};
    // int size=6;
    int arr[200];
    int size=0;
    int max=0;
    bool moved=false;
    int repeats = 0;

    cin >> repeats;

    for(int tryNumb=0; tryNumb<repeats; tryNumb++)
    {
        max=0;
        cin >> size;
        //read values
        for(int field=0; field<size; field++)
        {
            cin >> arr[field];
        }
        
        //find max value
        for(int field=0; field<size; field++)
        {
            if(max<arr[field])
            {
                max = arr[field];
            }
        }
    
        //sort, moving max vaules up, rest
        do
        {
            moved=false;
            for(int field=0; field<size; field++)
            {
                if(max==arr[field])
                {
                    if(field>0 && arr[field-1]<arr[field])
                    {
                        swap(arr[field-1],arr[field]);
                        moved = true;
                    }
                }
                else
                {
                    if(field>0 && arr[field-1]>arr[field] && max!=arr[field-1])
                    {
                        swap(arr[field-1],arr[field]);
                        moved = true;
                    }
                }
            }
        }while(true == moved);
        
        //print out sorted values
        for(int field=0; field<size; field++)
        {
            cout << arr[field];
            if(field<(size-1))
            {
                cout << " ";                
            }
        }
        if(tryNumb<(repeats-1))
        {
             cout << endl;
        }
    }
    return 0;
}