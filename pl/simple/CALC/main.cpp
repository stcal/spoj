#include <iostream>

#define STACK_SIZE 10u

using namespace std;

int main() 
{
	char cCode;
    int iArgA=0;
    int iArgB=0;
    int iResult=0;

	while(cin >> cCode)
	{
	    cin >> iArgA;
	    cin >> iArgB;
	    
        switch(cCode)
        {
            case '+':
                iResult = iArgA + iArgB;
                break;
            case '-':
                iResult = iArgA - iArgB;
                break;
            case '*':
                iResult = iArgA * iArgB;
                break;
            case '/':
                iResult = iArgA / iArgB;
                break;
            case '%':
                iResult = iArgA % iArgB;
                break;
        }
        
        cout << iResult << endl;
	}

	return 0;
}