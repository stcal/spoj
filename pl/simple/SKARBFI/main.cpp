#include <iostream>

using namespace std;

int main()
{
    int iTestsNumber=0;
    int iMoves=0;
    int wid=0;
    int len=0;
    int iDir;
    int iDis;
    
    cin >> iTestsNumber;
    
    for(int iTest=0;iTest<iTestsNumber;iTest++)
    {
        wid=0;
        len=0;
        cin >> iMoves;
        for(int iMove=0; iMove<iMoves; iMove++)
        {
            cin >> iDir;
            cin >> iDis;
            switch (iDir)
            {
                case 0:
                wid -= iDis;
                break;
                case 1:
                wid += iDis;
                break;
                case 2:
                len -= iDis;
                break;
                case 3:
                len += iDis;
                break;
                default:
                break;
            }
        }
        
        if(0==wid && 0==len)
        {
            cout << "studnia" << endl;
        }
        else
        {
            if(0>wid)
            {
                cout << "0 " << (wid*-1) << endl; 
            }
            else if(0<wid)
            {
                cout << "1 " << wid << endl; 
            }
            
            if(0>len)
            {
                cout << "2 " << (len*-1) << endl; 
            }
            else if(0<len)
            {
                cout << "3 " << len << endl; 
            }
        }
    }

    return 0;
}