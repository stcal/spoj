 #include <iostream> 
 #include <cstring>  

 using namespace std;  

 #define T_SIZE 1001  
char* string_merge(char *, char *);  

int main()
{
    int t,n;
    char S1[T_SIZE], S2[T_SIZE], *S;
    cin >> t; /* wczytaj liczbę testów */
    cin.getline(S1,T_SIZE);
    while(t)
    {
         cin.getline(S1,T_SIZE,' ');
         cin.getline(S2,T_SIZE);
         S=string_merge(S1,S2);
         cout << S << endl;
         delete[] S;
         t--;
    }
    return 0; 
}

char* string_merge(char *S1, char *S2)
{
    char Out[2*T_SIZE];
    bool end = false;
    char temp1;
    char temp2;
    int iInIterator = 0;
    int iOutIterator = 0;
    
    while(false == end)
    {
        temp1 = S1[iInIterator];
        temp2 = S2[iInIterator];
        if('\0'==temp1 || '\0'==temp2)
        {
            Out[iOutIterator] = '\0';
            end = true;
            break;
        }
        Out[iOutIterator] = temp1;
        iOutIterator++;
        Out[iOutIterator] = temp2;
        iOutIterator++;
        iInIterator++;
    }
    printf("%s\n", Out );
    return Out;
}
