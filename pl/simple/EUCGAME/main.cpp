#include <iostream>

using namespace std;

uint32_t play(uint32_t aP1, uint32_t aP2)
{
    uint32_t out=aP1+aP2;
    while(aP1!=aP2)
    {
        if(aP1<aP2)
        {
            aP2-=aP1;
            out-=aP1;
        }
        else
        {
            aP1-=aP2;
            out-=aP2;
        }
    }
    return out;
}

int main()
{
    int iGames=0;
    uint32_t iP1;
    uint32_t iP2;
    
    cin >> iGames;
    
    for(int game=0; game<iGames; game++)
    {
        cin >> iP1;
        cin >> iP2;
        cout << play(iP1,iP2) << endl;
    }

    return 0;
}