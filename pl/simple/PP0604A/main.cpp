#include <iostream>

using namespace std;

float midVal(int cnt, int *arr)
{
    float out=0;
    for(int i=0; i<cnt; i++)
    {
        out+=arr[i];
    }
    out=out/cnt;
    return out;
}

int main()
{
    int iTestsNumber;
    int iArray[100];
    int iElementNumber=0;
    float fMidVal=0;
    float fDiff=0;
    float fMinDiff=0;
    int iClosestElement=0;

    cin >> iTestsNumber;
    
    for(int iTest=0; iTest<iTestsNumber; iTest++)
    {
        
        fDiff=0;
        fMinDiff=0;
        iClosestElement=0;
    
        cin >> iElementNumber;
        
        for(int iElement=0; iElement<iElementNumber; iElement++)
        {
            cin >> iArray[iElement];
        }
        
        fMidVal = midVal(iElementNumber, iArray);
        
        for(int iElement=0; iElement<iElementNumber; iElement++)
        {
            if(0==iElement)
            {
                fMinDiff = (static_cast<float>(iArray[iElement])>fMidVal)? static_cast<float>(iArray[iElement])-fMidVal : fMidVal-static_cast<float>(iArray[iElement]);
            }
            else
            {
                fDiff = (static_cast<float>(iArray[iElement])>fMidVal)? static_cast<float>(iArray[iElement])-fMidVal : fMidVal-static_cast<float>(iArray[iElement]);
                if(fDiff<fMinDiff)
                {
                    fMinDiff = fDiff;
                    iClosestElement = iElement;
                }
            }
        }
        // cout << "Srednia: " << fMidVal << endl;
        cout << iArray[iClosestElement] << endl;
        
    }

    return 0;
}