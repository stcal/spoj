#include <iostream>

#define STACK_SIZE 10u

using namespace std;

int main() 
{
	char cCode;
	int iStack[STACK_SIZE];
	uint8_t u8Curr=0u;
	
	while(cin >> cCode)
	{
	    if('+'==cCode)
	    {
	        if(STACK_SIZE>u8Curr)
	        {
	            cin >> iStack[u8Curr];
	            u8Curr++;
	            cout << ":)" << endl;
	        }
	        else
	        {
	            cin >> cCode;
	            cout << ":(" << endl;
	        }
	    }
	    else if('-'==cCode)
	    {
	        if(0<u8Curr)
	        {
                u8Curr--;
    	        cout << iStack[u8Curr] << endl;
	        }
	        else
	        {
	            cout << ":(" << endl;
	        }

	    }
	    else
	    {
            cout << ":(" << endl;
	    }
	}

	return 0;
}