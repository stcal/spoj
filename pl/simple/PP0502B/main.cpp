#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int iNumberOfTests;
    int iLenght;
    int iTemp;
    vector<int> v_iNumbers[100];
    
    cin >> iNumberOfTests;
    
    for(int iTest=0; iTest<iNumberOfTests; iTest++)
    {
        cin >> iLenght;
        for(int iValue=0; iValue<iLenght; iValue++)
        {
            cin >> iTemp;
            v_iNumbers[iTest].push_back(iTemp);
        }
    }
    
    for(int iTest=0; iTest<iNumberOfTests; iTest++)
    {
        for(int iValue=(v_iNumbers[iTest].size()-1); iValue>=0; iValue--)
        {
            cout << v_iNumbers[iTest][iValue] << " ";
        }
        cout << endl;
    }
    return 0;
}