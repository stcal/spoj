#include <iostream>

using namespace std;



int main()
{
    int iTestsNumber;
    int arrayIn[100][100];
    int arrayOut[100][100];
    int x=0;
    int y=0;
    int n=0;
    int k=0;
    
    cin >> iTestsNumber;
    
    for(int iTest=0; iTest<iTestsNumber; iTest++)
    {
        cin >> n;
        cin >> k;
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<k;j++)
            {
                cin >> arrayIn[i][j];
            }
        }
        
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<k;j++)
            {
                if(0==j||0==i||(k-1)==j||(n-1)==i)
                {
                    //TOP LINE
                    if(0==j&&(n-1)!=i)
                    {
                        x=i+1;
                        y=j;
                    }
                    //BOTTOM LINE
                    else if((k-1)==j&&0!=i)
                    {
                        x=i-1;
                        y=j;
                    }
                    //LEFT LINE
                    else if(0==i&&0!=j)
                    {
                        x=i;
                        y=j-1;
                    }
                    //RIGHT LINE
                    else //if((n-1)==i&&(k-1)!=j)
                    {
                        x=i;
                        y=j+1;
                    }
                }
                else
                {
                    x=i;
                    y=j;
                }
                arrayOut[x][y] = arrayIn[i][j];
            }
        }
    
        for(int i=0;i<n;i++)
        {
            for(int j=0;j<k;j++)
            {
                cout << arrayOut[i][j] << " ";
            }
            cout << endl;
        }
    }

    return 0;
}