#include <iostream>
#include <string>

using namespace std;

int main() 
{
    int iTests;
    int iVel1;
    int iVel2;
    int iOut;

    cin >> iTests;
    
    for(int iTest=0;iTest<iTests; iTest++)
    {
        cin >> iVel1;
        cin >> iVel2;
        
        iOut = 2*iVel1*iVel2/(iVel1+iVel2);
        cout << iOut << endl;
    }

	return 0;
}