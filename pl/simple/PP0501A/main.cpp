#include <iostream>
#include <vector>

using namespace std;

struct aPair
{
    int m_iA;
    int m_iB;
};

int nwd(int a_iA, int a_iB)
{
    int iNWD;
    int iMin;
    int iMax;
    
    if( a_iA > a_iB )
    {
        iMax = a_iA;
        iMin = a_iB;
    }
    else
    {
        iMax = a_iB;
        iMin = a_iA;
    }
    iNWD = iMin;
    
    for(int iRepeat=1; 0!=iMax%iNWD; /*iRepeat++*/)
    {
    	do
    	{
    		iRepeat++;
    	}
    	while(0!=iMin%iRepeat);
    	iNWD = iMin/iRepeat;
    }
    
    return iNWD;
}

int main()
{
    int iNumberOfTests;
    aPair sTempValues;
    vector<aPair> v_sTempValues;
    
    cin >> iNumberOfTests;

    for(int iTest=0; iTest<iNumberOfTests; iTest++)
    {
        cin >> sTempValues.m_iA;
        cin >> sTempValues.m_iB;
        v_sTempValues.push_back(sTempValues);
    }

    for(aPair sValues : v_sTempValues)
    {
    	iNumberOfTests = nwd(sValues.m_iA,sValues.m_iB);
        cout << iNumberOfTests << endl;
    }

    return 0;
}