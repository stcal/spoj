#define ANSI
#include <iostream>
#include <cstring>

using namespace std;

void printfloat (float);

int main ()
{
    int t;
    float x;
    cin >> t;			/* wczytaj liczbD testC3w */
    while (t)
    {
        cin >> x;
        printfloat (x);
        t--;
    }
    return 0;
}				/* ........................  Tu napisz potrzebne funkcje */


void printfloat (float x)
{
    int sign=0;
    int exP=0;
    int fract=0;
    
    float femp=0.0;
    femp = x;
    
    int temp = 0;
    int temp2 = 0;
    temp = (int)x;
    
    //determine sign
    if(0.0 > x)
    {
        sign = 1;
        temp = temp * (-1);
        femp = femp * (-1.0);
    }
    
    //determine exponenta
    if(temp>1)
    {
        while(((int)temp)>1&&exP<127)
        {
            exP++;
            temp = temp >> 1;
        }
    }
    else
    {
        while(((int)temp)<1&&exP>-127)
        {
            exP--;
            temp = temp << 1;
        }
    }
    
    //determine mantisa
    if(23>exP)
    {
        for(int i=(23-exP); i>0; i--)
        {
            femp = femp * 2.0;
        }
    }
    else
    {
        for(int i=(exP-23); i>0; i--)
        {
            femp = femp / 2.0;
        }
    }
    fract = (int)femp;
    exP += 127;
    
    // temp=0;
    temp = ((sign&1) << 31) | ((exP&0xff) << 23) | ((fract&0x7FFFFF));

    // printf("%01b",sign&1);
    // printf("%08b",exP&0xff);
    // printf("%023b\n",fract&0x7FFFFF);    
    // printf("%032b\n", temp);
    // printf("%08x\n", temp);

    for(int i=3; i>=0; i--)
    {
        temp2 = (temp >> (i*8)) & 0xff;
        printf("%x ", temp2);
    }
    printf("\n");
}