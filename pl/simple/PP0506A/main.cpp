#include <iostream>

using namespace std;

struct point
{
    char name[11];
    int x;
    int y;
};

int radious(point arg)
{
    int out;
    out = arg.x*arg.x+arg.y*arg.y;
    return out;
}

void sort(point *table, int size)
{
    point temp;
    bool sorted=false;
    for(int i = 0; i<size; i++)
    {
        sorted=true;
        for(int j = 0; j<size-1; j++)
        {
            if(radious(table[j])>radious(table[j+1]))
            {
                temp = table[j];
                table[j] = table[j+1];
                table[j+1] = temp;
                sorted = false;
            }
        }
        if(true == sorted)
        {
            break;
        }
    }
}

int main()
{
    int iTestsNumber = 0;
    int iPointsNumber = 0;
    point allPoints[1000];
    
    cin >> iTestsNumber;

    for(int iTest=0; iTest<iTestsNumber; iTest++)
    {
        cin >> iPointsNumber;
        
        //scan points
        for(int iPoint = 0; iPoint<iPointsNumber; iPoint++)
        {
            cin >> allPoints[iPoint].name;
            cin >> allPoints[iPoint].x;
            cin >> allPoints[iPoint].y;
        }
        
        //sort points
        sort(allPoints, iPointsNumber);
        
        //print points
        for(int iPoint = 0; iPoint<iPointsNumber; iPoint++)
        {
            cout << allPoints[iPoint].name << " ";
            cout << allPoints[iPoint].x << " ";
            cout << allPoints[iPoint].y << endl;
        }
        cout << endl;
    }

    return 0;
}