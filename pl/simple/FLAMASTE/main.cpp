#include <iostream>
#include <string>
#include <sstream>

using namespace std;

void rebuildString(string &a_sWord)
{
    int iCount = 0;
    int iLength = a_sWord.length();
    char cTemp = '\0';
    ostringstream sOut;
    char cLetter;
    // bool addN;
    
    // for(char cLetter : a_sWord)
    for(int i=0; i<iLength; i++)
    {
        cLetter = a_sWord[i];
        if(cTemp == cLetter && (iLength-1) != i)
        {
            iCount++;
        }
        else
        {
            if(((iLength-1)==i) && (cTemp==cLetter) && (iCount>0))
            {
                iCount+=2;
                sOut << iCount;
            }
            else if(iCount>1)
            {
                iCount++;
                sOut << iCount;
                sOut << cLetter;
            }
            else if(iCount==1)
            {
                sOut << cTemp;
                sOut << cLetter;
            }
            else
            {
                sOut << cLetter;
            }
            iCount = 0;
        }
        cTemp = cLetter;
    }
    a_sWord = sOut.str();
}

int main()
{
    int iAmount = 0;
    string sWords[50];
    
    cin >> iAmount;
    
    for(int i=0; i<iAmount; i++)
    {
        cin >> sWords[i];
    }
    
    for(int i=0; i<iAmount; i++)
    {
        rebuildString(sWords[i]);
        cout << sWords[i] << endl;
    }
    
    return 0;
}