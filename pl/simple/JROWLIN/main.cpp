#include <iostream>

using namespace std;

void linear(float a, float b, float c)
{
    float result=0.0;
    
    if(0==a)
    {
        if(b!=c)
        {
            cout << "BR" << endl;
        }
        else
        {
            cout << "NWR" << endl;
        }
    }
    else
    {
        result = (c-b)/a;
        printf("%.2f\n",result);
    } 
}

int main()
{
    // cout<<"Hello World";

    float x;
    float y;
    float z;

    cin >> x;
    cin >> y;
    cin >> z;
    
    linear(x,y,z);

    return 0;
}