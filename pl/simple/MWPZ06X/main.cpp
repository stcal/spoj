#include <iostream>
using namespace std;

int main() {
	
	int iTries;
	int iSteps;
	
	cin >> iTries;
	for(int iTry=0; iTry<iTries; iTry++)
	{
		cin >> iSteps;
		cout << iSteps*iSteps << endl;
	}

	return 0;
}