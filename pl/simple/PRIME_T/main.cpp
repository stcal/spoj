#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int amount;
    int temp;
    vector<int> numbers;
    cin >> amount;
    
    for(int i = 0; i<amount; i++)
    {
        cin >> temp;
        numbers.push_back(temp);
    }
    
    for(int a : numbers)
    {
        bool has = false;
        if(1 == a)
        {
            has = true;    
        }
        for(int i=2; i<a; i++)
        {
            if(0 == a%i) 
            {
                has = true;
                exit;
            }
        }
        if(true == has)
        {
            cout << "NIE" << endl;
        }
        else
        {
            cout << "TAK" << endl;
        }
    }

    return 0;
}