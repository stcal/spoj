#include <iostream>
#include <math.h>  
using namespace std;

int main() {
	int iTries=0;
	int iX=0;
	int iY=0;
	int iZ=0;
	float iMonths=0;

	cin >> iTries;
	
	for(int iTry=0;iTry<iTries;iTry++)
	{
		cin >> iX;
		cin >> iY;
		cin >> iZ;
		
		iMonths = -12*(static_cast<float>(iX)/(static_cast<float>(iZ)-1.)-static_cast<float>(iY));
		cout << static_cast<int>(round(iMonths)) << endl;
	}

	return 0;
}