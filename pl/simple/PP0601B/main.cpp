#include <iostream>

using namespace std;

int main()
{
    int iAmount=0;
    int n=0;
    int x=0;
    int y=0;
    
    cin >> iAmount;
    
    for(int iTry=0; iTry<iAmount; iTry++)
    {
        cin >> n;
        cin >> x;
        cin >> y;
        for(int number=0;number<n;number++)
        {
            if((0==number%x)&&(0!=number%y))
            {
                cout << number << " ";
            }
        }
        cout << endl;
    }
    
    return 0;
}