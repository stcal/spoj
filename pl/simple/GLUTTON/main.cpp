#include <iostream>

#define TIME (int)60*60*24

using namespace std;


int eat(int speed)
{
    return (TIME/speed);
}

int main()
{
    int iTests;
    int iParticipants;
    int iBoxSize;
    int iTemp=0;
    int iTempS=0;
    int iBoxNumb = 0;
    
    cin >> iTests;
    
    for(int iTest=0; iTest<iTests; iTest++)
    {
        cin >> iParticipants;
        cin >> iBoxSize;
        iTemp = 0;
        
        for(int iParticipant=0; iParticipant<iParticipants;iParticipant++)
        {
            cin >> iTempS;
            iTemp+=eat(iTempS);
        }
        iBoxNumb = iTemp/iBoxSize + ((iTemp%iBoxSize!=0) ? 1 : 0);
        cout << iBoxNumb << endl;
    }
    
    return 0;
}