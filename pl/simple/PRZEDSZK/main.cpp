#include <iostream>

using namespace std;

struct aPair
{
    int a;
    int b;
};

int find(aPair a_sSet)
{
    int iMax=0;
    int iMin=0;
    int iOut=0;
    if(a_sSet.a > a_sSet.b)
    {
        iMax = a_sSet.a;
        iMin = a_sSet.b;
    }
    else
    {
        iMax = a_sSet.b;
        iMin = a_sSet.a;
    }
    
    iOut = iMax;
    for(int iMultiplier = 1; iOut%iMin!=0; iMultiplier++)
    {
        iOut = iMax*iMultiplier; 
    }
    return iOut;
}

int main()
{
    int iAmount = 0;
    aPair sAll[50];
    
    cin >> iAmount;
    
    for(uint8_t uSet=0u; uSet < iAmount; uSet++)
    {
        cin >> sAll[uSet].a;
        cin >> sAll[uSet].b;
    }

    for(uint8_t uSet=0u; uSet < iAmount; uSet++)
    {
        cout << find(sAll[uSet]) << endl;
    }
    
    return 0;
}