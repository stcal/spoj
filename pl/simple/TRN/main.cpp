#include <iostream>

using namespace std;

int main()
{
    int iM=0;
    int iN=0;
    
    int tab[200][200];
    
    cin >> iM;
    cin >> iN;

    for(int i=0; i<iM; i++)
    {
        for(int j=0; j<iN; j++)
        {
            cin >> tab[i][j];
        }
    }

    for(int j=0; j<iN; j++)
    {
        for(int i=0; i<iM; i++)
        {
            cout << tab[i][j] << " ";
        }
        cout << endl;
    }

    return 0;
}