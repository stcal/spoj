#include <iostream>
#include <vector>

using namespace std;

int main()
{
    int iTestsAmount = 0;
    int iSum = 0;
    int iTemp;
    int iNumbersAmount = 0;
    vector <int>vOut;
    
    cin >> iTestsAmount;
    for(int iTestNumber=0; iTestNumber<iTestsAmount; iTestNumber++)
    {
        cin >> iNumbersAmount;
        for(int iNumbersLeft=iNumbersAmount; iNumbersLeft>0; iNumbersLeft--)
        {
            cin >> iTemp;
            iSum += iTemp;
        }
        vOut.push_back(iSum);
        iSum = 0;
    }

    for(int a : vOut)
    {
        cout << a << endl;
    }

    return 0;
}