#include <iostream>

using namespace std;

int delta(float a, float b, float c)
{
    int ret = 0;
    float delta = 0;
    delta = b * b - 4 * a * c;
    
    if(0<delta)
    {
        ret = 2;
    }
    else if(0>delta)
    {
        ret = 0;
    }
    else
    {
        ret = 1;
    }
    
    return ret;
}

int main()
{
    float a=0;
    float b=0;
    float c=0;
    int out=0;
    
    while(cin >> a)
    {
        cin >> b;
        cin >> c;
        out = delta(a,b,c);
        cout << out << endl;
    }
    
    return 0;
}