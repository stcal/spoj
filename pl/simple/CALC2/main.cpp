#include <iostream>

#define STACK_SIZE 10u

using namespace std;

int main() 
{
	char cCode;
	int iRegister[10];
    int iArgA=0;
    int iArgB=0;
    int iResult=0;

	while(cin >> cCode)
	{
	    cin >> iArgA;
	    cin >> iArgB;
	    
	    if('z'==cCode)
	    {
	        iRegister[iArgA]=iArgB;
	    }
	    else
	    {
            switch(cCode)
            {
                case '+':
                    iResult = iRegister[iArgA] + iRegister[iArgB];
                    break;
                case '-':
                    iResult = iRegister[iArgA] - iRegister[iArgB];
                    break;
                case '*':
                    iResult = iRegister[iArgA] * iRegister[iArgB];
                    break;
                case '/':
                    iResult = iRegister[iArgA] / iRegister[iArgB];
                    break;
                case '%':
                    iResult = iRegister[iArgA] % iRegister[iArgB];
                    break;
            }
            cout << iResult << endl;	        
	    }
	}

	return 0;
}